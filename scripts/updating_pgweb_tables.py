#!/usr/bin/python3

import pandas as pd
from os import path
import os
import subprocess
import time
import wml.visionml as wv
import wml.visionml.view as wvv
conn = wv.get_muv1_conn()

path_folder = os.getcwd()
name_of_folder = os.path.basename(path_folder)

##droping the table
#start_drop_table = time.process_time()
print('Dropping the target_menu2group')
subprocess.call(['python3', '/home/malav/malav_ml/visionml-updater/muv1_grabber/recreate_muv1_derived_views.py', '--frame_name', 'target_menu2group', 'drop'])
print('Dropped the target_menu2group - Waiting for 10 sec to start the refresh')
#start_drop_table_t = str(((time.process_time() - start_drop_table)*100))
#print('Drop time taken= ' + str(((time.process_time() - start_drop_table)*100)))
time.sleep(5)

#data = [[str(name_of_folder) + '_drop_table', start_drop_table_t]]
#start_drop_table_pd = pd.DataFrame(data, columns=['script_name', 'time_taken'])
#start_drop_table_pd.to_sql('script_time_taken', conn, schema='ml', if_exists='append', index=False)

##Creating the drop table back in the view
#start_recreate_table = time.process_time()
print('Starting the process of recreating the table view')
subprocess.call(['python3', '/home/malav/malav_ml/visionml-updater/muv1_grabber/recreate_muv1_derived_views.py', 'create'])
print('Created the view again of the table after dropping target_menu2group')
#start_recreate_table_t = str((time.process_time() - start_recreate_table)*100)
#print('Creating table time taken = ' + str((time.process_time() - start_recreate_table)*100))
time.sleep(5)

#data = [[str(name_of_folder) + '_create_table', start_recreate_table_t]]
#start_recreate_table_pd = pd.DataFrame(data, columns=['script_name', 'time_taken'])
#start_recreate_table_pd.to_sql('script_time_taken', conn, schema='ml', if_exists='append', index=False)

##Refreshing the view
#start_refresh1 = time.process_time()
print('Refreshing the view')
subprocess.call(['python3', '/home/malav/malav_ml/visionml-updater/muv1_grabber/recreate_muv1_derived_views.py', 'refresh'])
print('View Refreshed - now creating filling bin crop')
#start_refresh1_t = str((time.process_time() - start_refresh1)*100)
#print('refresh time1  = ' + str((time.process_time() - start_refresh1)*100))
time.sleep(5)

#data = [[str(name_of_folder) + '_refresh_table1', start_refresh1_t]]
#start_refresh1_pd = pd.DataFrame(data, columns=['script_name', 'time_taken'])
#start_refresh1_pd.to_sql('script_time_taken', conn, schema='ml', if_exists='append', index=False)

##Filling bin crop - 1st time
#start_bin_fill1 = time.process_time()
print('Starting bin fill crop - 1st time')
subprocess.call(['python3', '/home/malav/malav_ml/munet-augmentor/scripts/munet_augmentor_fill_bin_crop.py'])
print('finished the process of bin fill crop')
#start_bin_fill1_t = str((time.process_time() - start_bin_fill1)*100)
#print('bin fill time1 = ' + str((time.process_time() - start_bin_fill1)*100))
time.sleep(5)

#data = [[str(name_of_folder) + '_bin_fill1', start_bin_fill1_t]]
#start_bin_fill1_pd = pd.DataFrame(data, columns=['script_name', 'time_taken'])
#start_bin_fill1_pd.to_sql('script_time_taken', conn, schema='ml', if_exists='append', index=False)

##Refreshing the view
#start_refresh2 = time.process_time()
print('Refreshing the view')
subprocess.call(['python3', '/home/malav/malav_ml/visionml-updater/muv1_grabber/recreate_muv1_derived_views.py', 'refresh'])
print('View Refreshed - now creating filling bin crop')
#start_refresh2_t = str((time.process_time() - start_refresh2)*100)
#print('refresh time2  = ' + str((time.process_time() - start_refresh2)*100))
time.sleep(5)

#data = [[str(name_of_folder) + '_refresh_table2', start_refresh2_t]]
#start_refresh2_pd = pd.DataFrame(data, columns=['script_name', 'time_taken'])
