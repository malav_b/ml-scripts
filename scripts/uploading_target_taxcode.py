#!/usr/bin/python3

import argparse
import pandas as pd
import dataframemt.psql as dp
import wml.visionml as wv
import wml.visionml.view as wvv
conn = wv.get_muv1_conn()
import time
from os import path
import os
from wml.core import logger

def main(args):

    time1 = time.process_time()
    if path.exists('./class_list.csv'):
        csv_file = './class_list.csv'
    else:
        csv_file = args.csv

    menu_group = 23
    t1 = pd.read_csv(csv_file)
    logger.info('menu_group_id = {}'.format(menu_group))

    df_final = pd.DataFrame(columns=['weight'])

    if 'weight' in t1.columns:
        df_final['weight'] = t1['weight'].copy()
        if 'transaction_count' in t1.columns:
            df_final['transaction_count'] = t1['transaction_count'].copy()
            df_final['transaction_count'] = df_final['transaction_count'].astype('int32')

        else:
            df_final['transaction_count'] = df_final['weight'] * 10000
            df_final['transaction_count'] = df_final['transaction_count'].astype('int32')
    if 'taxcode' in t1.columns:
        df_final['taxcode'] = t1['taxcode'].copy()
    if 'tax_code' in t1.columns:
        df_final['tax_code'] = t1['tax_code'].copy()
        df_final = df_final.rename(columns={"tax_code": "taxcode"})
    print(df_final.head())
    if 'menu_group_id' in t1.columns:
        df_final['menu_group_id'] = t1['menu_group_id'].copy()
    else:
        df_final['menu_group_id'] = df_final['transaction_count']*0
        df_final['menu_group_id'] = df_final['menu_group_id'] + menu_group

    #logger.info('df_final.head() = {} '.format(df_head.head()))
    df_final = df_final[['menu_group_id', 'taxcode', 'transaction_count', 'weight']]
    dp.to_sql(df_final, 'target_taxcode_per_group_dist', conn, schema='ml', if_exists='gently_replace')

    time1_taken = (time.process_time() - time1) * 100
    print('time taken to run target taxcode per group dist script = ' + str((time.process_time() - time1) * 100))

    path1 = os.path.dirname(csv_file)
    path2 = os.path.abspath(path1)
    name_of_folder = os.path.basename(path2)
    #print(path1)
    #print(os.path.abspath(path1))
    #print(name_of_folder)
    data = [[str(name_of_folder)+'_class_list', time1_taken]]
    time_df = pd.DataFrame(data, columns=['script_name', 'time_taken'])
    time_df.to_sql('script_time_taken', conn, schema='ml', if_exists='append', index=False)

    res = conn.execute("INSERT INTO ml.group_param(eval_from, menu_group_id) VALUES ('2020-03-21T00:00:00Z' ,"+ str(menu_group) +" )")
    print("Added menu_group_id to group_param")
    res1 = conn.execute("update ml.target_group_dist set weight = 0 where weight=1")
    print("Setting the weight to 0 for the old menu_group_id")
    res2 = conn.execute("INSERT INTO ml.target_group_dist(menu_group_id, weight) VALUES ("+ str(menu_group) +" , 1)")
    print("Setting the weight to 1 for the current menu_group_id")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--csv', default='class_list.csv', type=str,
                        help="Path to the CSV file ")
    parser.add_argument('--menu_group', default=10, type=int, help='Menu group id for the training data')
    args = parser.parse_args()
    main(args)
