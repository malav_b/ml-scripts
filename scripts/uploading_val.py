#!/usr/bin/python3

import argparse
import pandas as pd
import dataframemt.psql as dp
import wml.visionml as wv
import wml.visionml.view as wvv
conn = wv.get_muv1_conn()
import time
from os import path
import os
from wml.core import logger

def main(args):

    time1 = time.process_time()
    if path.exists('./data/v2/val.csv'):
        csv_file = './data/v2/val.csv'
    else:
        csv_file = args.csv_val

    if path.exists('./v2_val/val_pred.csv'):
        csv_file1 = './v2_val/val_pred.csv'
    else:
        csv_file1 = args.csv_val_pred

    t1 = pd.read_csv(csv_file)
    t2 = pd.read_csv(csv_file1)

    path1 = os.path.dirname(csv_file)
    path2 = os.path.abspath(path1)
    name_of_folder = os.path.basename(path2)
    
    name_of_folder = 'euro_model_20200401_1'
    t1.to_sql(str(name_of_folder)+'_event_munet_val',conn,schema='ml')
    time1_taken = (time.process_time() - time1) * 100
    print('time taken to run val script = ' + str((time.process_time() - time1) * 100))

    data = [[str(name_of_folder)+'_event_munet_val', time1_taken]]
    time_df = pd.DataFrame(data, columns=['script_name', 'time_taken'])
    time_df.to_sql('script_time_taken', conn, schema='ml', if_exists='append', index=False)

    t2.to_sql(str(name_of_folder)+'_event_munet_val_pred',conn,schema='ml')
    time2_taken = (time.process_time() - time1) * 100
    print('time taken to run val_pred script = ' + str((time.process_time() - time1) * 100))

    data = [[str(name_of_folder)+'_event_munet_val_pred', time2_taken]]
    time_df = pd.DataFrame(data, columns=['script_name', 'time_taken'])
    time_df.to_sql('script_time_taken', conn, schema='ml', if_exists='append', index=False)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--csv_val', default='val.csv', type=str,
                        help="Path to the CSV file ")
    parser.add_argument('--csv_val_pred', default='val_pred.csv', type=str,
                        help="Path to the CSV file ")

    args = parser.parse_args()
    main(args)
