#!/usr/bin/python3

import argparse
import pandas as pd
import dataframemt.psql as dp
import wml.visionml as wv
import wml.visionml.view as wvv
conn = wv.get_muv1_conn()
import time
from os import path
import os
from wml.core import logger


def main(args):

    time1 = time.process_time()
    if path.exists('./extra_event_munet.csv'):
        csv_file = './extra_event_munet.csv'
    else:
        csv_file = args.csv

    t1 = pd.read_csv(args.csv)

    df_final = pd.DataFrame(columns=['taxcode'])
    if 'taxcode' in t1.columns:
        df_final['taxcode'] = t1['taxcode'].copy()
    if 'tax_code' in t1.columns:
        df_final['taxcode'] = t1['tax_code'].copy()
    if 'event_id' in t1.columns:
        df_final['event_id'] = t1['event_id'].copy()
    if 'after_image_id' in t1.columns:
        df_final['after_image_id'] = t1['after_image_id'].copy()
    if 'before_image_id' in t1.columns:
        df_final['before_image_id'] = t1['before_image_id'].copy()
    if 'segmented' in t1.columns:
        df_final['segmented'] = t1['segmented'].copy()

    df_final = df_final[['event_id', 'taxcode', 'after_image_id', 'before_image_id', 'segmented']]
    dp.to_sql(df_final, 'event_munet_extra_data', conn, schema='ml', if_exists='gently_replace')


    time1_taken = (time.process_time() - time1) * 100
    print('time taken to run extra_eveent_munet  script = ' + str((time.process_time() - time1) * 100))

    path1 = os.path.dirname(csv_file)
    path2 = os.path.abspath(path1)
    name_of_folder = os.path.basename(path2)
    data = [[str(name_of_folder)+'_extra_event_munet', time1_taken]]
    time_df = pd.DataFrame(data, columns=['script_name', 'time_taken'])
    time_df.to_sql('script_time_taken', conn, schema='ml', if_exists='append', index=False)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--csv', default='extra_event_munet.csv', type=str,
                        help="Path to the CSV file ")
    args = parser.parse_args()
    main(args)
